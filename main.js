// Creating Firebase Reference
var fireBase = new Firebase("https://propercoffee.firebaseio.com");
fireBase.onAuth(authDataCallback);

//detect current log in status
function authDataCallback(authData) {
  if (authData) {
    //if user is logged in show form, hide login status and log it
    console.log("User " + authData.uid + " is logged in with " + authData.provider);
    document.getElementById("coffeeform").style.display = "block";
    document.getElementById("loginstatus").style.display = "none";
  } else {
      //if user is logged out log it, hide form and tell user to log in
      console.log("User logged out");
      document.getElementById("loginstatus").innerHTML = "Please log in to add data";
      document.getElementById("loginstatus").style.display = "block";
      document.getElementById("coffeeform").style.display = "none";
    }
};

//if user presses log in button try to log in with facebook popup
function loginButtonPress(authData) {
            fireBase.authWithOAuthPopup("facebook", function(error, authData) {
            if (error) {
                console.log("Login Failed!", error);
            } else {
                console.log("Authenticated successfully with payload:", authData);
            }
        });
};

//if log out button is pressed log out
function logoutButtonPress() {
    fireBase.unauth();
};

//create shopList firebase reference
var shopList = new Firebase("https://propercoffee.firebaseio.com/shops");

//Creating Geofire Reference
var geoFire = new GeoFire(fireBase.child("geofire"));

//pushes new entry to Firebase
var newShopEntry = shopList.push();

//Function Runs when admin user submits form
function formSubmitted(form){
    //pulls info from form
    var shopName = form.shopName.value;
    var lat = parseFloat(form.lat.value);
    var lon = parseFloat(form.lon.value);
    var monOpening = form.montimeopening.value;
    var tueOpening = form.tuetimeopening.value;
    var wedOpening = form.wedtimeopening.value;
    var thuOpening = form.thutimeopening.value;
    var friOpening = form.fritimeopening.value;
    var satOpening = form.sattimeopening.value;
    var sunOpening = form.suntimeopening.value;
    var monClosing = form.montimeclosing.value;
    var tueClosing = form.tuetimeclosing.value;
    var wedClosing = form.wedtimeclosing.value;
    var thuClosing = form.thutimeclosing.value;
    var friClosing = form.fritimeclosing.value;
    var satClosing = form.sattimeclosing.value;
    var sunClosing = form.suntimeclosing.value;
    var phone = form.phone.value;
    var postcode = form.postcode.value;
    var addressline = form.addressline.value;
    var wifi = form.wifi.value;
    var link = form.link.value;

//sets information from form to Firebase under the child shops
newShopEntry.set({
    addressline: addressline,
    lat: lat,
    link: link,
    lon: lon,
    openingtime: {
        openingTimes: {
        mon: monOpening,
        tue: tueOpening,
        wed: wedOpening,
        thu: thuOpening,
        fri: friOpening,
        sat: satOpening,
        sun: sunOpening
        },
        closingTimes: {
        mon: monClosing,
        tue: tueClosing,
        wed: wedClosing,
        thu: thuClosing,
        fri: friClosing,
        sat: satClosing,
        sun: sunClosing
        }
    },
    phone: phone,
    postcode: postcode,
    shopName: shopName,
    wifi: wifi
});

//retreives the random key assigned to it by firebase
var shopKey = newShopEntry.key();

//retreives the new entry to firebase from Firebase using the unique key
shopList.child(shopKey).once("value", function(snapshot) {
    //snapshots the data inside the object found at the keys location
    var newChildSnapshot = (snapshot.val());
    
    //finds and creates variables for the longitude and latiude of the new entry
    var shopLon = newChildSnapshot.lon;
    var shopLat = newChildSnapshot.lat;
    
    //adds new entry to GeoFire with the same Unique key
    geoFire.set(shopKey, [shopLat, shopLon]).then(function() {
        console.log("Provided key has been added to GeoFire");
        }, function(error) {
        console.log("Error: " + error);
        });
    window.location.reload();   
});
};
